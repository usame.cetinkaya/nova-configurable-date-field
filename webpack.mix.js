const mix = require('laravel-mix')

require('./nova.mix')

mix
  .setPublicPath('dist')
  .js('resources/js/field.js', 'js')
  .sass('resources/css/field.scss', 'css')
  .css('./node_modules/flatpickr/dist/flatpickr.css', 'css')
  .vue({
    version: 3,
  })
  .options({
    terser: {
      extractComments: false,
    },
  })
  .nova('gabelbart/nova-configurable-date-field')
