<template>
  <DefaultField
    :errors="errors"
    :field="currentField"
    :full-width-content="fullWidthContent"
    :show-help-text="showHelpText"
  >
    <template #field>
      <div class="flex items-center">
        <input
          :id="currentField.uniqueKey"
          ref="input"
          v-model="inputValue"
          class="form-control form-input form-input-bordered"
          :class="errorClasses"
          :disabled="currentlyIsReadonly"
          :dusk="field.attribute"
          :max="currentField.max"
          :min="currentField.min"
          :name="field.name"
          :placeholder="cUseFlatpickr ? cFlatpickrPlaceholder : null"
          :step="currentField.step"
          :type="cInputTime ? 'datetime-local' : 'date'"
          @change="onChangeInput"
        >

        <span
          v-if="cShowDiff && cFormattedLifeDiff"
          class="ml-3"
        >
          {{ cFormattedLifeDiff }}
        </span>

        <em
          v-if="cShowTimeZone"
          class="ml-3"
        >
          {{ cTimeZoneNameFormatted }}
        </em>
      </div>
    </template>
  </DefaultField>
</template>

<script>
import {
  DependentFormField,
  HandlesValidationErrors,
} from 'laravel-nova'

import {
  DateTime,
} from 'luxon'
import flatpickr from 'flatpickr'
import flatpickrLocales from 'flatpickr/dist/l10n'

import {
  useDateFieldUtils,
} from 'js/composables'

export default {
  mixins: [HandlesValidationErrors, DependentFormField],

  setup: (props, context) => useDateFieldUtils(props, context, 'form'),

  data: () => ({
    inputValue: '',
    value: '',
    flatpickr: null,
  }),

  computed: {
    cUse12HrFormat () {
      return Intl.DateTimeFormat(navigator.language, { hour: 'numeric' })
        .resolvedOptions()
        .hourCycle === 'h12'
    },
    cFlatpickrLocale () {
      return flatpickrLocales[navigator.language.split('-').shift()] ?? flatpickrLocales.default
    },
    cFlatpickrPlaceholder () {
      const format = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
      }

      if (this.cInputTime) {
        format.hour = '2-digit'
        format.minute = '2-digit'
        if (this.cInputSeconds) {
          format.second = '2-digit'
        }
      }

      const formattedDate = Intl.DateTimeFormat(navigator.language, format)
        .format(new Date())

      return formattedDate.replace(/\d|A|P|M/gi, '-')
    },
    cValueTimezone () {
      return Nova.config('timezone')
    },
    cInputTime () {
      return !!this.cIntlDateFormatOptions.timeStyle
        || !!this.cIntlDateFormatOptions.hour
        || !!this.cIntlDateFormatOptions.minute
        || !!this.cIntlDateFormatOptions.second
        || !!this.cIntlDateFormatOptions.fractionalSecondDigits
    },
    cInputSeconds () {
      return this.cInputTime && this.currentField.step % 60 !== 0
    },
    cInputTimeFormat () {
      if (this.cInputTime) {
        return this.cInputSeconds ? 'HH:mm' : 'HH:mm:ss'
      } else {
        return null
      }
    },
    cFormattedLifeDiff () {
      const base = DateTime.fromISO(
        DateTime.now().setZone(this.cValueTimezone),
        { zone: this.cValueTimezone }
      )

      return this.value
        ? DateTime.fromISO(this.value)
          .setZone(this.cTimeZone)
          .setLocale(this.cLocale)
          .toRelative({ base })
        : ''
    },
    cIsFirefoxDesktop () {
      const ua = navigator.userAgent.toLowerCase()

      return ua.includes('firefox') && !ua.includes('mobile')
    },
    cIsSafariDesktop () {
      const ua = navigator.userAgent.toLowerCase()

      return ua.includes('safari') && !ua.includes('mobile')
    },
    cUseFlatpickr () {
      return (this.cIsFirefoxDesktop && this.currentField.enableFirefoxFlatpickr)
        || (this.cIsSafariDesktop && this.currentField.enableSafariFlatpickr)
    },
  },

  watch: {
    currentField: {
      immediate: true,
      deep: true,
      handler (value) {
        if (value) {
          this.updateField(value)
          if (this.cUseFlatpickr) {
            this.updateFlatpickrConfig()
          }
        } else {
          this.updateField(this.field)
        }

        if (this.cUseFlatpickr) {
          this.$nextTick(() => {
            if (value.visible && this.$refs.input) {
              this.initFlatPickr()
            } else if (!value.visible) {
              this.destroyFlatpickr()
            }
          })
        }
      },
    },
    cTimeZone (value) {
      if (this.value) {
        this.setInputValue(
          DateTime.fromISO(this.value, { zone: this.cValueTimezone })
            .setZone(value)
        )
      }
    },
  },

  mounted () {
    if (this.cUseFlatpickr && this.field.visible) {
      this.initFlatPickr()
    }
  },

  unmounted () {
    setTimeout(() => this.destroyFlatpickr(), 100)
  },

  methods: {
    setInitialValue () {
      if (this.currentField.value) {
        const isoDate = DateTime.fromISO(this.currentField.value || this.value, {
          zone: this.cValueTimezone,
        })

        this.value = isoDate.toString()

        this.setInputValue(isoDate.setZone(this.cTimeZone))
      }
    },

    toInputFormat (value) {
      const formattedDate = [value.toISODate()]
      if (this.cInputTimeFormat) {
        formattedDate.push(value.toFormat(this.cInputTimeFormat))
      }

      return formattedDate.join('T')
    },

    setInputValue (value) {
      this.inputValue = this.toInputFormat(value)
    },

    fill (formData) {
      this.fillIfVisible(formData, this.field.attribute, this.value || '')

      if (this.currentlyIsVisible && this.value) {
        this.setInputValue(
          DateTime.fromISO(this.value, { zone: this.cValueTimezone })
            .setZone(this.cTimeZone)
        )
      }
    },

    onChangeInput (event) {
      if (!this.cUseFlatpickr) {
        let value = event?.target?.value ?? event

        if (value) {
          value = DateTime.fromISO(value, { zone: this.cTimeZone })
        }

        this.onChangeValue(value)
      }
    },

    onChangeFlatpickr (selectedDates) {
      const value = DateTime.fromJSDate(selectedDates[0])
        .setZone(this.cTimeZone, { keepLocalTime: true })

      this.onChangeValue(value)
    },

    onChangeValue (value) {
      if (value) {
        this.value = value.setZone(this.cValueTimezone).toString()
      } else {
        this.value = ''
      }

      if (this.field) {
        this.emitFieldValueChange(this.field.attribute, this.value)
      }
    },

    initFlatPickr () {
      flatpickr.localize(this.cFlatpickrLocale)
      this.flatpickr = flatpickr(this.$refs.input, {
        enableTime: this.cInputTime,
        enableSeconds: this.cInputSeconds,
        formatDate: (date) => {
          return DateTime
            .fromJSDate(date)
            .toLocaleString(this.cIntlDateFormatOptions)
        },
        onChange: this.onChangeFlatpickr,
        onReady: function (dateObj, dateStr, instance) {
          const button = document.createElement('input')
          button.type = 'reset'
          button.onclick = function () {
            instance.clear()
            instance.close()
          }
          instance.calendarContainer.appendChild(button)
        },
        minDate: this.currentField.min ?? null,
        maxDate: this.currentField.max ?? null,
        time_24hr: !this.cUse12HrFormat,
        defaultDate: DateTime.fromISO(this.currentField.value)
          .setZone(this.cTimeZone)
          .toString(),
      })
    },
    destroyFlatpickr () {
      this.flatpickr?.destroy()
      this.flatpickr = null
    },
    updateFlatpickrConfig () {
      if (this.flatpickr) {
        this.flatpickr.config.minDate = this.currentField.min
        this.flatpickr.config.maxDate = this.currentField.max
        this.flatpickr.config.enableTime = this.cInputTime
        this.flatpickr.config.enableSeconds = this.cInputSeconds
      }
    },
  },
}
</script>
