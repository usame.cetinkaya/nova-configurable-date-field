import {
  computed,
  ref,
} from 'vue'

import {
  DateTime,
} from 'luxon'

const timeZoneNames = [
  'long',
  'short',
  'shortOffset',
  'longOffset',
  'shortGeneric',
  'longGeneric',
]

export const useDateFieldUtils = (props, context, type) => {
  const isDetailComponent = type === 'detail'
  const isIndexComponent = type === 'index'
  const isFormComponent = type === 'form'

  const field = computed(() => updatedField.value ?? props.field)
  const updatedField = ref(null)
  const updateField = (newValue) => {
    updatedField.value = newValue
  }

  const cComment = computed(() => field?.value.comment)
  const cHasComment = computed(() => !!cComment.value)
  const cShowCommentOnIndex = computed(() => cHasComment.value
    && !field?.value.hideCommentFromIndex)
  const cTimeZone = computed(() => field?.value.timezone
        || Nova.config('userTimezone')
        || Nova.config('timezone'))
  const cShowTimeZone = computed(() => {
    if (
      isIndexComponent
      && typeof field?.value.showTimezoneOnIndex === 'boolean'
    ) {
      return typeof field?.value.showTimezoneOnIndex
    } else if (
      isDetailComponent
      && typeof field?.value.showTimezoneOnDetail === 'boolean'
    ) {
      return field?.value.showTimezoneOnIndex
    } else if (
      isFormComponent
      && typeof field?.value.showTimezoneOnForms === 'boolean'
    ) {
      return typeof field?.value.showTimezoneOnForms === 'boolean'
    } else {
      return !!field?.value.showTimezone
    }
  })
  const cTimeZoneName = computed(() => {
    if (
      isIndexComponent
      && timeZoneNames.includes(field?.value.timezoneNameOnIndex)
    ) {
      return field?.value.timezoneNameOnIndex
    } else if (
      isDetailComponent
      && timeZoneNames.includes(field?.value.timezoneNameOnDetail)
    ) {
      return field?.value.timezoneNameOnDetail
    } else if (
      isFormComponent
      && timeZoneNames.includes(field?.value.timezoneNameOnForms)
    ) {
      return timeZoneNames.includes(field?.value.timezoneNameOnForms)
    } else if (timeZoneNames.includes(field?.value.timezoneName)) {
      return field?.value.timezoneName
    } else {
      return 'short'
    }
  })
  const cTimeZoneNameFormatted = computed(() => (new Intl.DateTimeFormat(cLocale.value, {
    year: 'numeric',
    timeZone: cTimeZone.value,
    timeZoneName: cTimeZoneName.value,
  })).format(new Date()).replace(/\d{4}.\s/, ''))
  const cLocale = computed(() => Nova.config('locale'))
  const cIntlDateFormatOptions = computed(() => {
    const options = field?.value.intlDateFormatOptions

    if (cShowTimeZone.value) {
      options.timeZoneName = cTimeZoneName.value
    }

    return options
  })
  const cUsesCustomizedDisplay = computed(() => {
    return field?.value.usesCustomizedDisplay
      && !!field?.value.displayedAs
  })
  const cDateValue = computed(() => DateTime.fromISO(field?.value.value)
    .setZone(cTimeZone.value)
    .setLocale(cLocale.value))
  const cFormattedDate = computed(() => cDateValue.value.toLocaleString(cIntlDateFormatOptions.value))
  const cFormattedDiff = computed(() => cDateValue.value.toRelative())
  const cShowDiff = computed(() => {
    if (
      isDetailComponent
      && typeof field?.value.showDiffOnDetail === 'boolean'
    ) {
      return typeof field?.value.showDiffOnDetail === 'boolean'
    } else if (
      isIndexComponent
      && typeof field?.value.showDiffOnIndex === 'boolean'
    ) {
      return typeof field?.value.showDiffOnIndex === 'boolean'
    } else if (
      isFormComponent
      && typeof field?.value.showDiffOnForm === 'boolean'
    ) {
      return field?.value.showDiffOnForm === 'boolean'
    } else {
      return !!field?.value.showDiff
    }
  })
  const cShowDiffAsPrimaryInformation = computed(() => !!field?.value.showDiffAsPrimaryInformation)
  const cFormattedValue = computed(() => {
    if (cUsesCustomizedDisplay.value) {
      return field?.value.displayedAs
    }

    const formattedDate = cFormattedDate.value
    if (cShowDiff.value) {
      const formattedDiff = cFormattedDiff.value
      return cShowDiffAsPrimaryInformation.value
        ? `${formattedDiff} (${formattedDate})`
        : `${formattedDate} (${formattedDiff})`
    } else {
      return formattedDate
    }
  })

  return {
    cComment,
    cFormattedDate,
    cFormattedDiff,
    cFormattedValue,
    cHasComment,
    cIntlDateFormatOptions,
    cLocale,
    cShowCommentOnIndex,
    cShowDiff,
    cShowDiffAsPrimaryInformation,
    cShowTimeZone,
    cTimeZone,
    cTimeZoneName,
    cTimeZoneNameFormatted,
    cUsesCustomizedDisplay,
    updateField,
  }
}
