import IndexField from './components/IndexField'
import DetailField from './components/DetailField'
import FormField from './components/FormField'

Nova.booting((app, store) => {
  app.component('IndexGabelbartDate', IndexField)
  app.component('DetailGabelbartDate', DetailField)
  app.component('FormGabelbartDate', FormField)
})
