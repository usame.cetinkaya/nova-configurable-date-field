const path = require('path')

const mix = require('laravel-mix')

const webpack = require('webpack')

class NovaExtension {
  name () {
    return 'nova-extension'
  }

  register (name) {
    this.name = name
  }

  webpackPlugins () {
    return new webpack.ProvidePlugin({
      _: 'lodash',
      Errors: 'form-backend-validation',
    })
  }

  webpackConfig (webpackConfig) {
    webpackConfig.externals = {
      vue: 'Vue',
    }

    webpackConfig.resolve.alias = {
      ...(webpackConfig.resolve.alias || {}),
      'laravel-nova': path.join(
        __dirname,
        'vendor/laravel/nova/resources/js/mixins/packages.js'
      ),
      luxon: path.join(
        __dirname,
        'vendor/laravel/nova/node_modules/luxon/build/amd/luxon.js'
      ),
      js: path.join(
        __dirname,
        'resources/js'
      ),
    }

    webpackConfig.output = {
      uniqueName: this.name,
    }
  }
}

mix.extend('nova', new NovaExtension())
