## 0.4.0 - 2023-03-28

**Features:**
- Add placeholder with flatpickr
- Add option to enable flatpickr in safari desktop

**Fixes:**
- Fix pointer style with flatpickr
- Fix error with diff on index
- Fix missing initial value

## 0.3.0 - 2023-03-22

**Features**
- Detect hour cycle style for flatpickr with browser settings
- Localize flatpickr based on browser locale

## 0.2.1 - 2023-03-22

**Fixes:**
- Fix flatpickr issue when field is hidden/shown via dependency

## 0.2.0 - 2023-03-21

**Features:**
- Ability to control behavior via global defaults, see `setDefaults` and `addDefaults` static methods
- Option to replace broken Firefox implementation for `datetime-local` with Flatpickr

## 0.1.2 - 2023-03-07

**Fixes:**
- Issue where field was reset on input

## 0.1.1 - 2023-02-25

**Fixes:**
- Issue with timezone on forms
- Update displayed form-value when timezone is updated via dependent field
- Fix formatted timezone name on forms

## 0.1.0 - 2023-02-24 -- Initial release
