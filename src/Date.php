<?php

namespace Gabelbart\Laravel\Nova\Fields\Date;

class Date extends \Laravel\Nova\Fields\Date
{
    use HasDateOptions;

    const FORMAT_DEFAULT = [
        'year' => 'numeric',
        'month' => '2-digit',
        'day' => '2-digit',
    ];

    public $component = 'gabelbart-date';

    public function __construct($name, $attribute = null, callable $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this
            ->intlDateFormatOptions(self::FORMAT_DEFAULT);

        $this->applyDateDefaults();
    }
}
