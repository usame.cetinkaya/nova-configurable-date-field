<?php

namespace Gabelbart\Laravel\Nova\Fields\Date;

class DateTime extends \Laravel\Nova\Fields\DateTime
{
    use HasDateOptions;

    const FORMAT_DEFAULT = [
        'year' => 'numeric',
        'month' => '2-digit',
        'day' => '2-digit',
        'hour' => '2-digit',
        'minute' => '2-digit',
    ];

    public $component = 'gabelbart-date';

    public function __construct($name, $attribute = null, callable $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this
            ->intlDateFormatOptions(static::FORMAT_DEFAULT);

        $this->applyDateDefaults();
    }
}
