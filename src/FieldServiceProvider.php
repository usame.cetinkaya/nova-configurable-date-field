<?php

namespace Gabelbart\Laravel\Nova\Fields\Date;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('gabelbart-date', __DIR__.'/../dist/js/field.js');
            Nova::style('gabelbart-date', __DIR__.'/../dist/css/field.css');
            Nova::style('gabelbart-date-flatpickr', __DIR__.'/../dist/css/flatpickr.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
