<?php

namespace Gabelbart\Laravel\Nova\Fields\Date;

use Illuminate\Support\Arr;

/**
 * @mixin \Laravel\Nova\Fields\Field
 */
trait HasDateOptions
{
    protected static $defaults = [];

    public static function setDefaults(array $defaults)
    {
        static::$defaults = $defaults;
    }

    public static function addDefaults(array $defaults)
    {
        static::$defaults = array_merge(
            $defaults,
            static::$defaults
        );
    }

    protected function applyDateDefaults()
    {
        foreach (static::$defaults as $key => $value) {
            if (method_exists($this, $key)) {
                $this->{$key}(...Arr::wrap($value));
            }
        }
    }

    public function enableFirefoxFlatpickr(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function enableSafariFlatpickr(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function intlDateFormatOptions(array $options): self
    {
        $this->withMeta([__FUNCTION__ => $options]);

        return $this;
    }

    public function timezone(?string $timezone): self
    {
        $this->withMeta([__FUNCTION__ => $timezone]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function showTimezone(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function showTimezoneOnIndex(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function showTimezoneOnDetail(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function showTimezoneOnForms(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function timezoneName(string $name): self
    {
        $this->withMeta([__FUNCTION__ => $name]);

        return $this;
    }

    public function timezoneNameOnIndex(string $name): self
    {
        $this->withMeta([__FUNCTION__ => $name]);

        return $this;
    }

    public function timezoneNameOnDetail(string $name): self
    {
        $this->withMeta([__FUNCTION__ => $name]);

        return $this;
    }

    public function timezoneNameOnForms(string $name): self
    {
        $this->withMeta([__FUNCTION__ => $name]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function showDiff(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function showDiffOnDetail(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function showDiffOnIndex(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function showDiffAsPrimaryInformation(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }

    public function comment(?string $comment = null): self
    {
        $this->withMeta(['comment' => $comment]);

        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function hideCommentFromIndex(bool $flag = true): self
    {
        $this->withMeta([__FUNCTION__ => $flag]);

        return $this;
    }
}
